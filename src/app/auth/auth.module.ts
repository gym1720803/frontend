import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SignupComponent } from './pages/signup/signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRepository } from './repositories/auth.repository';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [SignupComponent, LoginComponent],
  imports: [CommonModule, AuthRoutingModule, ReactiveFormsModule],
  providers: [
    {
      provide: AuthRepository,
      useClass: AuthService,
    },
  ],
})
export class AuthModule {}
