import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthRepository } from '../repositories/auth.repository';
import { Observable } from 'rxjs';
import { ISignup, IToken } from '../interfaces/signup.interface';
import { ILogin } from '../interfaces/login.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements AuthRepository {
  constructor(private _http: HttpClient) {}
  loginUser(data: ILogin): Observable<IToken> {
    return this._http.post<IToken>('/auth/login', data);
  }
  signupUser(data: ISignup): Observable<IToken> {
    return this._http.post<IToken>('/auth/signup', data);
  }
}
