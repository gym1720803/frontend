import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILoginForm } from '../../interfaces/login.interface';
import { FormValidationService } from '../../../shared/services/form-validation.service';
import { AuthRepository } from '../../repositories/auth.repository';
import { HttpErrorResponse } from '@angular/common/http';
import { WhoIAmService } from '../../../shared/services/whoiam.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public loginForm: FormGroup<ILoginForm>;
  constructor(
    private fb: FormBuilder,
    public _formValidation: FormValidationService,
    private _authRepository: AuthRepository,
    private _whoIAmService: WhoIAmService,
    private router: Router
  ) {
    this.loginForm = this.generateLoginForm();
    this._formValidation.setFormGroup = this.loginForm;
  }

  private generateLoginForm(): FormGroup<ILoginForm> {
    return this.fb.nonNullable.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  public loginUser(): void {
    this._authRepository.loginUser(this.loginForm.getRawValue()).subscribe({
      next: ({ token }) => {
        this._whoIAmService.setToken = token;
        this.router.navigateByUrl('/inicio');
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      },
    });
  }
}
