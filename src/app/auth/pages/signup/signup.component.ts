import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserSignup } from '../../interfaces/signup.interface';
import { FormValidationService } from '../../../shared/services/form-validation.service';
import { AuthRepository } from '../../repositories/auth.repository';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { WhoIAmService } from '../../../shared/services/whoiam.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  public signupForm: FormGroup<UserSignup>;

  constructor(
    private fb: FormBuilder,
    public _formValidation: FormValidationService,
    private _authRepository: AuthRepository,
    private router: Router,
    private _whoIAmService: WhoIAmService
  ) {
    this.signupForm = this.generateSignupForm();
    this._formValidation.setFormGroup = this.signupForm;
  }

  generateSignupForm() {
    return this.fb.nonNullable.group({
      name: ['', Validators.required],
      cellphone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      business: ['', Validators.required],
    });
  }

  registerUser() {
    if (!this._formValidation.isValidForm()) return;
    this._authRepository.signupUser(this.signupForm.getRawValue()).subscribe({
      next: (data) => {
        this._whoIAmService.setToken = data.token;
        this.router.navigateByUrl('/inicio');
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      },
    });
  }
}
