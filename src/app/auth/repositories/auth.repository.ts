import { Observable } from 'rxjs';
import { ISignup, IToken } from '../interfaces/signup.interface';
import { ILogin } from '../interfaces/login.interface';

export abstract class AuthRepository {
  abstract signupUser(data: ISignup): Observable<IToken>;
  abstract loginUser(data: ILogin): Observable<IToken>;
}
