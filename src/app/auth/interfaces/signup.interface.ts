import { FormControl } from '@angular/forms';

export interface UserSignup {
  name: FormControl<string>;
  cellphone: FormControl<string>;
  email: FormControl<string>;
  password: FormControl<string>;
  business: FormControl<string>;
}

export interface ISignup {
  name: string;
  cellphone: string;
  email: string;
  password: string;
}

export interface IToken {
  token: string;
  refresh?: string;
}

export interface IWhoIAm {
  name: string;
  email: string;
  role: string;
  isActive: boolean;
}
