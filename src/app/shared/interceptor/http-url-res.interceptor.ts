import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { WhoIAmService } from '../services/whoiam.service';

@Injectable()
export class HttpUrlResInterceptor implements HttpInterceptor {
  private baseURLapi: string;
  private listUrlToExclude = ['login', 'signup'];
  private token = '';

  constructor(private _whoIAmService: WhoIAmService) {
    this.baseURLapi = environment.backend;
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.token = this._whoIAmService.itemToken || 'undefined';
    const url = this.baseURLapi + request.url;
    if (this.excludeUrl(request.url)) {
      const reqClone = request.clone({ url });
      return next.handle(reqClone);
    }

    if (request.url.includes('/upload')) {
      let headers = new HttpHeaders({
        Authorization: `Bearer ${this.token}`,
      });
      const reqUpload = request.clone({ url, headers });
      return next.handle(reqUpload);
    }

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
    });

    const reqClone = request.clone({ url, headers });
    return next.handle(reqClone);
  }

  excludeUrl(url: string): boolean {
    return this.listUrlToExclude.some(
      (urlExclude) => url.includes(urlExclude) === true
    );
  }
}
