import { TestBed } from '@angular/core/testing';

import { HttpUrlResInterceptor } from './http-url-res.interceptor';

describe('HttpUrlResInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [HttpUrlResInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: HttpUrlResInterceptor = TestBed.inject(
      HttpUrlResInterceptor
    );
    expect(interceptor).toBeTruthy();
  });
});
