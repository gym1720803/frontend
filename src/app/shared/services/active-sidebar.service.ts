import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ActiveSidebarService {
  private toggleNav$ = new BehaviorSubject<boolean>(true);

  constructor() {}

  public get toggleValue(): Observable<boolean> {
    return this.toggleNav$.asObservable();
  }

  public set toggleChange(clickValue: boolean) {
    this.toggleNav$.next(clickValue);
  }
}
