import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormValidationService {
  private _formGroup!: FormGroup;

  public set setFormGroup(form: FormGroup) {
    this._formGroup = form;
  }

  public errorFormForValue(nameForm: string): boolean {
    return (this._formGroup.get(nameForm)?.invalid &&
      (this._formGroup.get(nameForm)?.dirty ||
        this._formGroup.get(nameForm)?.touched))!;
  }

  public isValidForm(): boolean {
    let existError = true;
    Object.keys(this._formGroup.controls).forEach((key) => {
      const err = this._formGroup.get(key)?.errors;
      if (err !== null) {
        this._formGroup.get(key)?.markAsTouched();
        existError = false;
      }
    });
    return existError;
  }

  public setResetFormGroup(): void {
    this._formGroup.reset();
  }
}
