import { TestBed } from '@angular/core/testing';

import { ActiveSidebarService } from './active-sidebar.service';

describe('ActiveSidebarService', () => {
  let service: ActiveSidebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActiveSidebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
