import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IWhoIAm } from '../../auth/interfaces/signup.interface';

@Injectable({
  providedIn: 'root',
})
export class WhoIAmService {
  private whoIAmData?: IWhoIAm;
  private instance?: Observable<IWhoIAm>;
  private token?: string = localStorage.getItem('token') || undefined;

  constructor(private http: HttpClient) {}

  getWhoIAmData(): IWhoIAm | undefined {
    return this.whoIAmData;
  }

  whoIAm(): Observable<IWhoIAm> {
    if (!this.instance)
      this.instance = this.http.get<IWhoIAm>('/auth/whoiam').pipe(
        tap({
          next: (value) => {
            this.whoIAmData = value;
          },
        })
      );
    return this.instance;
  }

  public get itemToken() {
    return this.token;
  }

  public set setToken(token: string) {
    this.token = token;
    localStorage.setItem('token', token);
  }

  logoutSession(): void {
    this.whoIAmData = undefined;
    this.instance = undefined;
    localStorage.clear();
    sessionStorage.clear();
  }
}
