import { Component } from '@angular/core';
import { ActiveSidebarService } from '../../../shared/services/active-sidebar.service';
import { WhoIAmService } from '../../../shared/services/whoiam.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  constructor(
    public readonly toggleSidebar: ActiveSidebarService,
    private _whoIAm: WhoIAmService,
    private _router: Router
  ) {}

  logoutUser() {
    this._whoIAm.logoutSession();
    this._router.navigateByUrl('/auth/ingreso');
  }
}
