import { Component } from '@angular/core';
import { ActiveSidebarService } from '../../../shared/services/active-sidebar.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  private toggleValue!: boolean;
  public toggleCart = false;
  private $subToggle: Subscription;

  constructor(public readonly toggleSidebar: ActiveSidebarService) {
    this.$subToggle = this.toggleSidebar.toggleValue.subscribe(
      (val) => (this.toggleValue = val)
    );
  }

  changeStatusSidebar() {
    this.toggleSidebar.toggleChange = !this.toggleValue;
  }

  ngOnDestroy(): void {
    this.$subToggle.unsubscribe();
  }

  updateTheme(theme: 'dark' | 'light') {
    document.documentElement.setAttribute('data-theme', theme);
  }
}
