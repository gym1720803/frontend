import { Component } from '@angular/core';
import { ActiveSidebarService } from '../shared/services/active-sidebar.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  constructor(public readonly toggleSidebar: ActiveSidebarService) {}
}
