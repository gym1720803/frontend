import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

import { catchError, map, of } from 'rxjs';
import { WhoIAmService } from '../../shared/services/whoiam.service';

export const AuthGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const _whoiamService = inject(WhoIAmService);
  const router = inject(Router);
  return _whoiamService.whoIAm().pipe(
    map((auth) => {
      if (auth.isActive) return true;
      else {
        _whoiamService.logoutSession();
        router.navigateByUrl('/auth/ingreso');
        return false;
      }
    }),
    catchError((err) => {
      _whoiamService.logoutSession();
      router.navigateByUrl('/auth');
      return of(false);
    })
  );
};
